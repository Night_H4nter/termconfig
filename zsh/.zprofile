#pretend it's KDE to have icons
# export XDG_CURRENT_DESKTOP=KDE
#export QT_QPA_PLATFORMTHEME=qt5ct

# export XDG_SESSION_TYPE=x11
# export GDK_BACKEND=x11

#mostly for sudoedit
export EDITOR=nvim
#colored output
export LS_COLORS="$LS_COLORS:ow=1;7;34:st=30;44:su=30;41"
[ -f "/home/daniel/.ghcup/env" ] && source "/home/daniel/.ghcup/env" # ghcup-env

export PATH="/home/daniel/.dotfiles/termconfig/scripts/:/home/daniel/.local/bin:$PATH"
#lua stuff
#export LUA_PATH="/usr/share/awesome/lib/?.lua;/usr/share/awesome/lib/?/init.lua;/usr/share/awesome/lib/?/?.lua;;$LUA_PATH"
# export LUA_CPATH="/home/daniel/repos/tym-3.1.2/?/src/?.o$LUA_CPATH"
# export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
# source "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"


#xdg default stuff
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"


#xdg (non-)compliance stuff (thanks https://github.com/b3nj5m1n/xdg-ninja)
export ANSIBLE_HOME="$XDG_DATA_HOME/ansible"
export CABAL_CONFIG="$XDG_CONFIG_HOME/cabal/config"
export CABAL_DIR="$XDG_CACHE_HOME/cabal"
export KDEHOME="$XDG_CONFIG_HOME/kde"
export STACK_ROOT="$XDG_DATA_HOME/stack"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export WORKON_HOME="$XDG_DATA_HOME/virtualenvs"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export GTK_RC_FILES="$XDG_CONFIG_HOME/gtk-1.0/gtkrc"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
# export WINEPREFIX="$XDG_DATA_HOME/wine"
export GOPATH="$XDG_DATA_HOME/go"
export NODE_REPL_HISTORY="$XDG_DATA_HOME/node_repl_history"
export NVM_DIR="$XDG_DATA_HOME/nvm"
export PARALLEL_HOME="$XDG_CONFIG_HOME/parallel"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export PYENV_ROOT="$XDG_DATA_HOME/pyenv"
export SQLITE_HISTORY="$XDG_DATA_HOME/sqlite_history"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export MACHINE_STORAGE_PATH="$XDG_DATA_HOME/docker-machine"
export FFMPEG_DATADIR="$XDG_CONFIG_HOME/ffmpeg"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export NUGET_PACKAGES="$XDG_CACHE_HOME/NuGetPackages"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgrep/config"
export PLTUSERHOME="$XDG_DATA_HOME/racket"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"
alias nvidia-settings='nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings'
alias svn='svn --config-dir "$XDG_CONFIG_HOME/subversion"'
alias yarn='yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config"'
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias ncmpc=ncmpc -f "$XDG_CONFIG_HOME/ncmpc/config"
alias feh='alias --no-fehbg'


# export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker" #this breaks podman


# i know i'm an idiot and i'll forget these are set, so comment it out
# alias conky='conky --config="$XDG_CONFIG_HOME/conky/conkyrc"' 
# alias dosbox='dosbox -conf "$XDG_CONFIG_HOME/dosbox/dosbox.conf"'


#zsh requires this to be in /etc/zsh/zshenv
# ZDOTDIR=$HOME/.config/zsh
# this should be in .zshrc
# compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

# this should be in $PREFIX/etc/npmrc
# prefix=${XDG_DATA_HOME}/npm
# cache=${XDG_CACHE_HOME}/npm
# tmp=${XDG_RUNTIME_DIR}/npm
# init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js




# automatically start x when logging in
#  if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#    exec startx
#  fi
