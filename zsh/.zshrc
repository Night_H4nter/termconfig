# The following lines were added by compinstall


#autoconf{{{
zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' list-colors ''
# zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:]}={[:upper:]}'
zstyle :compinstall filename '/home/daniel/.zshrc'


autoload -Uz compinit
compinit
# End of lines added by compinstall


# Lines configured by zsh-newuser-install
HISTFILE=~/.zshhist
HISTSIZE=10000
SAVEHIST=10000
bindkey -e
# End of lines configured by zsh-newuser-install
# }}}


#git branch
function git_branch_name()
{
    branch=$(git symbolic-ref HEAD 2> /dev/null | awk 'BEGIN{FS="/"} {print $NF}')
    if [[ $branch == "" ]];
    then
        :
    else
        echo ' '$branch' '
    fi
}


#user/root
function currentuser() {
    if [[ $whoami == "root" ]];
    then
        echo '%B%F{red}root%f%b'
    else
        echo '%B%F{cyan}$%f%b'
    fi
}


#enable substitution in the prompt
setopt prompt_subst


#move by word
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word


#fix del key
bindkey  "^[[3~"  delete-char


alias stow='stow -v'
alias ls='ls --color=auto'
alias nc='cd ~/.dotfiles/devconfig/userconf/nvim/.config/nvim/ && nvim'
alias dc='cd ~/.dotfiles/deconfig/ && nvim'
alias tc='cd ~/.dotfiles/termconfig/userconf/ && nvim'
alias r='cd ~/repos/ && ls'
alias start-xephyr='Xephyr -br -ac -noreset -screen 1280x720 :2 &'
alias kill-xephyr='killall Xephyr'
alias test-stumpwm='DISPLAY=:2 ~/repos/stumpwm/stumpwm.ros &'
alias kill-stumpwm='killall sbcl'
alias test-xmonad='DISPLAY=:2 xmonad &'
alias kill-xmonad='killall xmonad'


# function nvim {
#    /usr/bin/env nvim "$@" && clear
# }


##prompt
PS1="%B%F{white}╭─ %f%F{magenta}%~%f %F{blue}"'$(git_branch_name)'"%f"$'\n'"%F{white}╰──%f%b "'$(currentuser)'" "


#autostart tmux
# [ -z "$TMUX"  ] && { tmux attach || exec tmux new-session && exit;}
